package model;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import java.util.Date;

import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="APPUSER")
public class Appuser {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Size(min=3, max=50)
	@Column(name = "FIRSTNAME", nullable = false)
	private String firstname;

	@NotNull
	@DateTimeFormat(pattern="dd.MM.yyyy") 
	@Column(name = "BIRTHDATE", nullable = false)
	private Date birthdate;

	//1. TODO Properties
	//2. TODO getters and setters
		
}
